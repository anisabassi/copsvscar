﻿using UnityEngine;

namespace CarVsCops
{
    public class EndlessTerrain : MonoBehaviour
    {
        private Transform groundObject;
        public float groundTextureSpeed = -0.4f;

        private void Start()
        {
            if (groundObject == null)
                groundObject = this.transform;
        }

        private void LateUpdate()
        {
            // If there is a ground object make its UV map move based on the player position ( which gives a feeling of movement on the fround )
            if (groundObject && PlayerSharedData.instance)
            {
                // Keep the ground object follwing the player position
                groundObject.position = new Vector3(PlayerSharedData.instance.gameObject.transform.position.x, 0, PlayerSharedData.instance.gameObject.transform.position.z);

                // Update the texture UV of the ground based on the player position
                groundObject.GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(PlayerSharedData.instance.gameObject.transform.position.x, PlayerSharedData.instance.gameObject.transform.position.z) * groundTextureSpeed);
            }
        }
    }
}
