﻿using System.Collections.Generic;
using UnityEngine;

namespace CarVsCops
{
    /// <summary>
    /// Manage Spawning Different Collectible Arround Player
    /// Note : This script was inspired from the internet
    /// </summary> Spawning 
    public class CollectibleSpawningManager : MonoBehaviour
    {
        [Header("Collectible Configuration")]
        // The maximum and minimum radius to spawn arround player
        public Vector2 spawnMinMaxRadius;
        // Collectible list to spawn
        public List<ElementToSpawn> elementsToSpawn = new List<ElementToSpawn>();
        // Instance of the current script
        public static CollectibleSpawningManager instance { get; private set; }


        private void OnEnable()
        {
            // Subscire to GameStart Event
            GameManager.OnGameStart += GameStarted;
        }

        private void OnDisable()
        {
            // Unsubscribe to GameStart Event
            GameManager.OnGameStart -= GameStarted;
        }

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(this);
            }
        }

        private void GameStarted()
        {
            // Start Spawing on Game Sart Event
            StartSpawning();
        }

        /// <summary>
        /// Spawn Collectible based of there number to spawn in the configuration list
        /// </summary>
        public void StartSpawning()
        {
            // check list is not empty
            if (elementsToSpawn == null)
                return;

            for (int i = 0; i < elementsToSpawn.Count; i++)
            {
                for (int j = 0; j < elementsToSpawn[i].numberOfObjectToSpawn; j++)
                {
                    SpawnObject(elementsToSpawn[i].PrefabToSpawn, elementsToSpawn[i].objectHeightFromFloor, elementsToSpawn[i].localScale);
                }
            }

        }


        /// <summary>
        /// Spawn New Object Around Player
        /// </summary>
        /// <param name="spawnObject">Object to spawn</param>
        /// <param name="spawnHeight">Spawning Y axis Height</param>
        /// <param name="scale">Spawning Scale</param>
        public void SpawnObject(GameObject spawnObject, float spawnHeight, Vector3 scale)
        {

            // Get postion to spawn
            Vector3 tempRandomPos = RandomCircle(PlayerSharedData.instance.transform.position, Random.Range(spawnMinMaxRadius.x, spawnMinMaxRadius.y)); 

            tempRandomPos.y = spawnHeight;

            GameObject instance = ((GameObject)(Instantiate(spawnObject, transform.position, transform.rotation)));

            instance.transform.localScale = scale;

            instance.transform.localPosition = tempRandomPos;

            instance.gameObject.GetComponent<AbilityTrigger>().distanceMax = (spawnMinMaxRadius.y / 2);

        }

        /// <summary>
        /// Call This method to spawn new collectible based on there ability type 
        /// </summary>
        /// <param name="abilityToSpawn">Ability Type</param>
        public void SpawnNewCollectible(SpecialAbility.Ability abilityToSpawn)
        {
            for (int i = 0; i < elementsToSpawn.Count; i++)
            {
                if (elementsToSpawn[i].CurrentAbility == abilityToSpawn)
                {
                    SpawnObject(elementsToSpawn[i].PrefabToSpawn, elementsToSpawn[i].objectHeightFromFloor, elementsToSpawn[i].localScale);
                }
            }
        }

        /// <summary>
        /// Return a point arround center with giving radius 
        /// </summary>
        /// <param name="center">Center of Circle </param>
        /// <param name="radius">Radius</param>
        /// <returns></returns>
        private Vector3 RandomCircle(Vector3 center, float radius)
        {
            float ang = Random.value * 360;
            Vector3 pos;
            pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
            pos.y = 0f;
            pos.z = center.z + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
            return pos;
        }
    }

    /// <summary>
    /// Sub class that contains collectible settings
    /// </summary>
    [System.Serializable]
    public class ElementToSpawn
    {
        public SpecialAbility.Ability CurrentAbility;
        public GameObject PrefabToSpawn;
        public int numberOfObjectToSpawn;
        public float objectHeightFromFloor;
        public Vector3 localScale;
    }
}