﻿using UnityEngine;

namespace CarVsCops
{
    public class PlayerCarCanvasManager : MonoBehaviour
    {
        public GameObject playerCarCanvasParent;
        public GameObject carNearPlayerPanel;
        public GameObject specialAbilityPanel;
        public GameObject specialAbilityCopsRadar;
        public enum CanvasElements { HideAll, CarNearPlayer, LaserBeamAbility }

        public CanvasElements canvasElements;

        // Start is called before the first frame update
        private void Start()
        {
            ResetCanvasData(false, false, false);
        }

        public void ActiveCanvasElements(CanvasElements sentCanvasElements)
        {

            switch (sentCanvasElements)
            {
                case CanvasElements.HideAll:
                    ResetCanvasData(false, false, false);
                    break;

                case CanvasElements.CarNearPlayer:
                    ResetCanvasData(true, true, false);
                    break;

                case CanvasElements.LaserBeamAbility:
                    ResetCanvasData(true, false, true);
                    break;
            }
        }

        private void ResetCanvasData(bool isCarCanvas, bool isCarNear, bool isSpecailAbility)
        {
            if (playerCarCanvasParent != null)
                playerCarCanvasParent.SetActive(isCarCanvas);
            if (carNearPlayerPanel != null)
                carNearPlayerPanel.SetActive(isCarNear);
            if (specialAbilityPanel != null)
            {
                specialAbilityPanel.SetActive(isSpecailAbility);
                specialAbilityCopsRadar.SetActive(isSpecailAbility);
            }
        }

        public void KillAllCopsAbility()
        {
            if (specialAbilityCopsRadar.GetComponent<FieldOfView>() != null)
            {
                if (specialAbilityPanel != null)
                {
                    specialAbilityPanel.SetActive(false);
                }
                specialAbilityCopsRadar.GetComponent<FieldOfView>().KillAll();
            }
        }
    }
}