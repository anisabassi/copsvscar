﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
public class ProgressBarUI : MonoBehaviour
{
    public Image currentProgressBar;
    private float currentAmount;
    public float currentProgressBarFillingTime;
    public bool isBarFull;
    public UnityEvent OnFilled;
    void OnEnable()
    {
        ResetProgressBar();
    }

    void OnDisable()
    {
        isBarFull = false;
        ResetProgressBar();
    }

    // Update is called once per frame
    private void Update()
    {
        if (!isBarFull && currentProgressBar != null)
        {
            FillProgressBar();
        }
    }

    private void FillProgressBar()
    {
        currentAmount += Time.unscaledDeltaTime;
        float percent = currentAmount / currentProgressBarFillingTime;
        currentProgressBar.fillAmount = Mathf.Lerp(0, 1, percent);

        if (currentProgressBar.fillAmount >= 1)
        {
            isBarFull = true;
            OnFilled.Invoke();
        }
    }

    private void ResetProgressBar()
    {
        if (currentProgressBar != null)
            currentProgressBar.fillAmount = 0f;
        currentAmount = 0f;
    }

}
