﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CarVsCops
{
    public class GetScoreUI : MonoBehaviour
    {

        public ScoreManager.ScoreType scoreType;
        private int score;
        private Text text;
        // Start is called before the first frame update
        void Start()
        {
            if (text == null)
                text = this.gameObject.GetComponent<Text>();
        }

        // Update is called once per frame
        void Update()
        {
            if( ScoreManager.instance)
            {
                score = ScoreManager.instance.GetScore(scoreType);
                if (text != null)
                    text.text = score.ToString();
            }
        }


    }
}
