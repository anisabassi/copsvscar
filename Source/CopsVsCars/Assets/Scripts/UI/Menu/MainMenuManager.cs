﻿using UnityEngine;
using UnityEngine.UI;

namespace CarVsCops
{
    public class MainMenuManager : MonoBehaviour
    {
        public enum Panels { Main , EndGame , InGame}
        public Panels panels;
        public GameObject joystickInput;
        public GameObject abilityPanel;
        public GameObject mainPanel;
        public GameObject endGamePanel;
        public GameObject inGamePanel;
        public ProgressBarUI laserBeamProgress;
        public static MainMenuManager instance { get; private set; }
        public SetLifesUI playerLifes;
        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(this);
            }
        }

        public void ShowSpecialAbilityPanel(bool isActive, float duration)
        {
            if (abilityPanel != null)
            {
                abilityPanel.SetActive(isActive);
                laserBeamProgress.currentProgressBarFillingTime = duration;

                if (joystickInput!=null)
                    joystickInput.SetActive(false);


            }
        }

        public void ActiveSpecialAbility()
        {

            PlayerSharedData.instance.gameObject.GetComponent<SpecialAbility>().ActiveLaserBeamAbility();

            if (joystickInput != null)
                joystickInput.SetActive(true);
        }

        public void SenToUiNewLife(bool isAddLife)
        {
            if (playerLifes)
                playerLifes.SetUILife(isAddLife);
        }

        public void SetPanelsVisibility(Panels panels)
        {
            switch (panels)
            {
                case Panels.Main:
                    PanelsVisibility(true,false,false);
                    break;
                case Panels.EndGame:
                    PanelsVisibility(false, true, false);
                    break;
                case Panels.InGame:
                    PanelsVisibility(false, false, true);
                    break;
                default:
                    break;
            }


        }

        private void PanelsVisibility(bool isMain, bool isEndGame , bool isInGame )
        {
            if (mainPanel)
                mainPanel.SetActive(isMain);

            if (endGamePanel)
                endGamePanel.SetActive(isEndGame);

            if (inGamePanel)
                inGamePanel.SetActive(isInGame);

            /*if (joystickInput != null)
                joystickInput.SetActive(isInGame);*/

        }


    }
}