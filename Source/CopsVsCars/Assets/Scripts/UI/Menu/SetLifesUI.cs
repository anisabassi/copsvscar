﻿using UnityEngine;

namespace CarVsCops
{
    public class SetLifesUI : MonoBehaviour
    {
        public GameObject[] lifes;
        private int lifeCounter = 0;
        //public int maxLife = 3;
        private void Start()
        {
            if (lifes == null)
                return;

            lifeCounter = lifes.Length - 1;

            for (int i = 0; i < lifes.Length; i++)
            {
                lifes[i].SetActive(true);
            }
        }

        public void SetUILife(bool isAddLife)
        {
            if (lifes == null || lifeCounter < 0 || lifeCounter >= lifes.Length)
                return;

            if (isAddLife)
            {
                lifeCounter++;

                lifes[lifeCounter].SetActive(true);
            }
            else
            {
                lifeCounter--;

                lifes[lifeCounter].SetActive(false);
            }
        }
    }
}