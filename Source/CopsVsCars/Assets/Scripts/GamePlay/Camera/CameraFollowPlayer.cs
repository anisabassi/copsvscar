﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarVsCops
{
    /// <summary>
    /// Camera FollowPlayer 
    /// Note : This script was inspired from the internet
    /// </summary>
    public class CameraFollowPlayer : MonoBehaviour
    {
        //Offset distance between the player and camera
        private Vector3 offset;            

        // Use this for initialization
        void Start()
        {
            //Calculate and store the offset value by getting the distance between the player position and camera position.
            offset = transform.position - PlayerSharedData.instance.transform.position;
        }

        // LateUpdate is called after Update each frame
        void LateUpdate()
        {
            if (PlayerSharedData.instance != null)
            {
                // Set the position of the camera to follow plyaer but with offset.
                transform.position = PlayerSharedData.instance.transform.position + offset;
            }
        }
    }
}
