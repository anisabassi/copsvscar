﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace CarVsCops
{
    /// <summary>
    /// Cops Car Manager
    /// </summary>
    public class GameManager : MonoBehaviour
    {
        public static GameManager instance { get; private set; }
        public bool isGameStarted;

        public delegate void StartGame();

        public static event StartGame OnGameStart;

        public int targetFrameRate = 180;

        [Header("Difficulty Configuration")]
        public int scoreMediumDifficulty;

        public int scoreHardDifficulty;
        private bool ischangeDifficultyToMedium = false;
        private bool ischangeDifficultyToHard = false;

        private void Awake()
        {
            Application.targetFrameRate = targetFrameRate;

            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(this);
            }
        }

        private void Start()
        {
            if (MainMenuManager.instance)
                MainMenuManager.instance.SetPanelsVisibility(MainMenuManager.Panels.Main);
        }

        private void Update()
        {
            // Change Game Difficulty base on car crashing score
            if (isGameStarted)
            {
                ChangeDifficultyBaseOnScore();
            }
        }

        /// <summary>
        /// On Game Start
        /// </summary>
        public void GameStart()
        {
            isGameStarted = true;
            MainMenuManager.instance.SetPanelsVisibility(MainMenuManager.Panels.InGame);

            ScoreManager.instance.SetLifes(PlayerSharedData.instance.gameObject.GetComponent<PlayerHealth>().currentHitsTaken);
            OnGameStart();
        }

        /// <summary>
        /// On Game End
        /// </summary>
        public void GameEnd()
        {
            isGameStarted = false;
            MainMenuManager.instance.SetPanelsVisibility(MainMenuManager.Panels.EndGame);
        }

        /// <summary>
        /// On Main Menu
        /// </summary>
        public void MainMenu()
        {
            isGameStarted = false;
            MainMenuManager.instance.SetPanelsVisibility(MainMenuManager.Panels.Main);
        }

        /// <summary>
        /// On RestartGame
        /// </summary>
        public void RestartGame()
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }

        /// <summary>
        /// Change Difficulty
        /// </summary>
        private void ChangeDifficultyBaseOnScore()
        {
            // The Game Start On Easy Level
            /*if (ScoreManager.instance.carCrashingScore == scoreEasyDifficulty)
            {
                if (!isWaitNextScoreToChangeDifficulty)
                {
                    CopsGenerator.instance.ChangeDifficultyLevel(CopsGenerator.DifficultyLevel.Easy);
                    isWaitNextScoreToChangeDifficulty = true;
                }
                return;
            }*/

            if (ScoreManager.instance.carCrashingScore >= scoreMediumDifficulty && !ischangeDifficultyToMedium)
            {
                CopsCarSpawningManager.instance.ChangeDifficultyLevel(CopsCarSpawningManager.DifficultyLevel.Medium);
                ischangeDifficultyToMedium = true;
            }

            if (ScoreManager.instance.carCrashingScore >= scoreHardDifficulty && !ischangeDifficultyToHard)
            {
                CopsCarSpawningManager.instance.ChangeDifficultyLevel(CopsCarSpawningManager.DifficultyLevel.Hard);
                ischangeDifficultyToHard = true;
            }
        }
    }
}