﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarVsCops
{
    /// <summary>
    /// Cops Generator
    /// </summary>
    public class CopsCarSpawningManager : MonoBehaviour
    {
        public static CopsCarSpawningManager instance { get; private set; }

        [Header("Cops Car Configuration")]
        public CopsCarManager CopsCarPrefab;

        public int spawnCount = 0;
        public int maxObjectsInScene = 1;
        public float spawnRadius;
        public float spawnInterval;
        public bool isPlayerDead;

        public List<DifficultyLevelPrefabs> carSettings = new List<DifficultyLevelPrefabs>();

        public List<int> idCarsSettings = new List<int>();

        public enum DifficultyLevel { Easy, Medium, Hard }

        public DifficultyLevel currentDifficultyLevel;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(this);
            }

            ChangeDifficultyLevel(currentDifficultyLevel);
        }

        private void Update()
        {
            // Check if didn't reach max cars then spawn new car
            if (GameManager.instance.isGameStarted)
            {
                if (maxObjectsInScene > spawnCount && !isPlayerDead)
                {
                    StartCoroutine("SpawnNewCopsCar");
                }
            }
        }

        /// <summary>
        /// Spawn New Car
        /// </summary>
        /// <returns></returns>

        private IEnumerator SpawnNewCopsCar()
        {
            if (idCarsSettings == null && CopsCarPrefab.gameObject == null)
                yield break;

            spawnCount++;
            yield return new WaitForSecondsRealtime(spawnInterval);

            Vector3 tempRandomPos = RandomCircle(PlayerSharedData.instance.transform.position, spawnRadius);

            GameObject obj = ((GameObject)(Instantiate(CopsCarPrefab.gameObject, transform.position, transform.rotation)));
            obj.GetComponent<CopsCarManager>().currentCarID = idCarsSettings[Random.Range(0, idCarsSettings.Count)];
            obj.transform.parent = this.transform;
            obj.transform.localPosition = tempRandomPos;
            obj.SetActive(true);
        }

        /// <summary>
        /// Return a point arround center with giving radius
        /// </summary>
        /// <param name="center">Center of Circle </param>
        /// <param name="radius">Radius</param>
        /// <returns></returns>
        private Vector3 RandomCircle(Vector3 center, float radius)
        {
            float ang = Random.value * 360;
            Vector3 pos;
            pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
            pos.y = 0f;
            pos.z = center.z + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
            return pos;
        }

        /// <summary>
        /// Change Difficulty Level
        /// </summary>
        /// <param name="sentDifficultyLevel"></param>
        public void ChangeDifficultyLevel(DifficultyLevel sentDifficultyLevel)
        {
            Debug.Log(" Chaning");
            if (carSettings == null)
                return;

            Debug.Log(" stop");

            StopAllCoroutines();

            Debug.Log(" change");

            currentDifficultyLevel = sentDifficultyLevel;

            for (int i = 0; i < carSettings.Count; i++)
            {
                if (carSettings[i].difficultyLevel == sentDifficultyLevel)
                {
                    idCarsSettings.Clear();
                    idCarsSettings = carSettings[i].carSettings;
                    maxObjectsInScene = carSettings[i].numberOfObjectToSpawn;
                    return;
                }
            }
        }

        /// <summary>
        /// Sub class that contains cops car settings
        /// </summary>
        [System.Serializable]
        public class DifficultyLevelPrefabs
        {
            public CopsCarSpawningManager.DifficultyLevel difficultyLevel;
            public List<int> carSettings = new List<int>();
            public int numberOfObjectToSpawn;
        }
    }
}