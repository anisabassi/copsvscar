﻿using UnityEngine;

namespace CarVsCops
{
    /// <summary>
    /// Save/Set Score
    /// </summary>
    public class ScoreManager : MonoBehaviour
    {
        public static ScoreManager instance { get; private set; }

        public enum ScoreType { Coins, CarCarshed, Life }

        public ScoreType scoreType;

        [Header("Score Configuration")]
        public int coinsCollected;
        public int carCrashingScore;

        public int carCrashingScoreValue;
        public int carCrashingScoreModifier;

        private int tempScore;


        private int currentLifes;
        private float time;

        private void Awake()
        {

            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(this);
            }

            // Get Saved Score From Player Prefs
            if (PlayerPrefs.HasKey("CoinsCollected") == false)
            {
                PlayerPrefs.SetInt("CoinsCollected", coinsCollected);
            }
            else
            {
                coinsCollected = PlayerPrefs.GetInt("CoinsCollected");
            }

        }


        public void AddCoinsScore(int sentCoins)
        {

            coinsCollected += sentCoins;
            PlayerPrefs.SetInt("CoinsCollected", coinsCollected);
        }

        private int GetCollectedCoins()
        {
            return coinsCollected;
        }

        private void SetCollectedCoins(int coins)
        {
            PlayerPrefs.SetInt("CoinsCollected", coins);
        }


        /// <summary>
        /// Add Score When Car Crushed
        /// </summary>
        public void AddCarCrashingScore()
        {
            if (!GameManager.instance.isGameStarted)
                return;

            if (carCrashingScoreModifier == 0)
                carCrashingScoreModifier = 1;


            carCrashingScore += (carCrashingScoreValue * carCrashingScoreModifier);
        }

        private int GetCarCrashingScore()
        {
            return carCrashingScore;
        }

        private void SetCarCrashingScore(int coins)
        {
            carCrashingScore += coins;
        }

        /// <summary>
        /// Add New Life
        /// </summary>
        /// <param name="isAddLife"></param>

        public void SetLifes(int sentLife)
        {
            currentLifes = sentLife;
            /*if (MainMenuManager.instance)
                MainMenuManager.instance.SenToUiNewLife(isAddLife);*/
        }

        private int GetLife()
        {
            return currentLifes;
        }


        /// <summary>
        /// Get Score Based On There Type
        /// </summary>
        /// <param name="scoreType">Score Type</param>
        /// <returns></returns>
        public int GetScore(ScoreType scoreType)
        {
            switch (scoreType)
            {
                case ScoreType.Coins:
                    tempScore = GetCollectedCoins();
                    break;

                case ScoreType.CarCarshed:
                    tempScore = GetCarCrashingScore();
                    break;

                case ScoreType.Life:
                    tempScore = GetLife();
                    break;
            }

            return tempScore;
        }
    }
}