﻿using System.Collections;
using UnityEngine;

namespace CarVsCops
{
    /// <summary>
    /// Special Ability behavior
    /// Note : This script was inspired from the internet
    /// </summary>
    public class SpecialAbility : MonoBehaviour
    {
        public enum Ability { Nitro, Life, LaserBeam, Coins }

        [Header("Ability Configuration")]
        public Ability abillity;

        public LayerMask specialAbilityLayerMask;
        public PlayerCarCanvasManager playerCarCanvasManager;
        private float abilityDuration;

        private PlayerHealth playerHeath;
        private PlayerMovement playerMovement;
        private bool activeAbilityOneAtTheTime = true;

        private void Start()
        {
            if (playerCarCanvasManager == null && this.gameObject.GetComponent<PlayerCarCanvasManager>() != null)
                playerCarCanvasManager = this.gameObject.GetComponent<PlayerCarCanvasManager>();

            if (playerHeath == null && this.gameObject.GetComponent<PlayerHealth>() != null)
                playerHeath = this.gameObject.GetComponent<PlayerHealth>();

            if (playerMovement == null && this.gameObject.GetComponent<PlayerMovement>() != null)
                playerMovement = this.gameObject.GetComponent<PlayerMovement>();
        }

        /// <summary>
        /// Set Collected Ability Configuration 
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerEnter(Collider other)
        {
            if (((1 << other.gameObject.layer) & specialAbilityLayerMask) != 0)
            {
                if (other.gameObject.GetComponent<AbilityTrigger>())
                {
                    switch (other.gameObject.GetComponent<AbilityTrigger>().CurrentAbility)
                    {
                        case Ability.Nitro:
                            SpeedBooster(other.gameObject.GetComponent<AbilityTrigger>().boostSpeedToAdd, other.gameObject.GetComponent<AbilityTrigger>().abilityDuration);
                            break;

                        case Ability.Life:
                            AddLife();
                            break;

                        case Ability.LaserBeam:
                            abilityDuration = other.gameObject.GetComponent<AbilityTrigger>().abilityDuration;
                            StartCoroutine(FindTargetsWithDelay(abilityDuration));
                            break;

                        case Ability.Coins:
                            AddCoins(other.gameObject.GetComponent<AbilityTrigger>().coinsToAdd);
                            break;

                        default:
                            break;
                    }

                    activeAbilityOneAtTheTime = false;
                }
            }
        }

        /// <summary>
        /// Player Can Collect new Ability
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerExit(Collider other)
        {
            if (((1 << other.gameObject.layer) & specialAbilityLayerMask) != 0)
            {
                if (other.gameObject.GetComponent<AbilityTrigger>())
                {
                    activeAbilityOneAtTheTime = true;
                }
            }
        }

        #region LaserBeam

        /// <summary>
        /// Active Laser Beam Abilirt
        /// </summary>
        public void ActiveLaserBeamAbility()
        {
            if (playerCarCanvasManager != null)
                playerCarCanvasManager.KillAllCopsAbility();

            StopAllCoroutines();
            SlowMotion(false);
        }

        private IEnumerator FindTargetsWithDelay(float delay)
        {
            SlowMotion(true);
            yield return new WaitForSecondsRealtime(delay);
            SlowMotion(false);
        }

        /// <summary>
        ///Slow Motion And Show Ability UI
        /// </summary>
        /// <param name="isActive"></param>
        private void SlowMotion(bool isActive)
        {
            if (isActive)
            {
                Time.timeScale = 0.1f;
                Time.fixedDeltaTime = 0.02F * Time.timeScale;
                playerCarCanvasManager.ActiveCanvasElements(PlayerCarCanvasManager.CanvasElements.LaserBeamAbility);
                MainMenuManager.instance.ShowSpecialAbilityPanel(true, abilityDuration);
            }
            else
            {
                Time.timeScale = 1;
                Time.fixedDeltaTime = 0.02F;
                //playerCarCanvasManager.ActiveCanvasElements(PlayerCarCanvasManager.CanvasElements.HideAll);
                MainMenuManager.instance.ShowSpecialAbilityPanel(false, abilityDuration);
            }
        }

        #endregion LaserBeam

        /// <summary>
        /// Add New Coins
        /// </summary>
        /// <param name="sentCoins">Coins To Add</param>
        private void AddCoins(int sentCoins)
        {
            if (ScoreManager.instance)
            {
                ScoreManager.instance.AddCoinsScore(sentCoins);
            }
        }


        /// <summary>
        /// Add New Life
        /// </summary>
        private void AddLife()
        {
            if (playerHeath)
            {
                playerHeath.AddLife();
            }
        }

        /// <summary>
        /// Boost Speed for a period of time
        /// </summary>
        /// <param name="speedToAd">Boost Speed</param>
        /// <param name="duration">Duration </param>
        private void SpeedBooster(float speedToAd, float duration)
        {

            if (playerMovement)
                playerMovement.ActiveBooster(speedToAd, duration);
        }
    }
}