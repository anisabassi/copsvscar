﻿using System.Collections;
using UnityEngine;

namespace CarVsCops
{
    /// <summary>
    /// Player Mouvement
    /// Note : This script was inspired from the internet
    /// </summary>
    public class PlayerMovement : CarMovement
    {
        public float booster = 1f;

        public VariableJoystick variableJoystick;

        private float xMovementRightJoystick;
        private float zMovementRightJoystick;

        private void Update()
        {
            // Get Joystick Values
            xMovementRightJoystick = variableJoystick.Horizontal;
            zMovementRightJoystick = variableJoystick.Vertical;
        }

        /// <summary>
        /// Override Car Roation
        /// </summary>
        public override void RotateCar()
        {
            // Calculate the player's direction based on angle
            float tempAngle = Mathf.Atan2(zMovementRightJoystick, xMovementRightJoystick);
            xMovementRightJoystick *= Mathf.Abs(Mathf.Cos(tempAngle));
            zMovementRightJoystick *= Mathf.Abs(Mathf.Sin(tempAngle));

            // Rotate the player to face the direction of input
            Vector3 temp = transform.position;
            temp.x += xMovementRightJoystick;
            temp.z += zMovementRightJoystick;
            Vector3 lookDirection = temp - transform.position;
            if (lookDirection != Vector3.zero)
            {
                this.transform.localRotation = Quaternion.Slerp(this.transform.localRotation, Quaternion.LookRotation(lookDirection), carSteeringRoationSpeed * Time.deltaTime);
            }
        }

        /// <summary>
        /// Add Speed Booster
        /// </summary>
        /// <param name="boosterValue">Boost Value</param>
        /// <param name="duration">Duration</param>
        public void ActiveBooster(float boosterValue, float duration)
        {
            StopCoroutine("BoostSpeed");

            StartCoroutine(BoostSpeed(boosterValue, duration));
        }

        /// <summary>
        /// Boost Speed
        /// </summary>
        /// <param name="Value">Boost Value</param>
        /// <param name="delay">Duration</param>
        /// <returns></returns>
        private IEnumerator BoostSpeed(float Value, float delay)
        {
            if (Value < 0)
                Value = 1f;

            carSpeed += Value;
            yield return new WaitForSecondsRealtime(delay);
            carSpeed -= Value;
        }
    }
}