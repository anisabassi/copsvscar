﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarVsCops
{
    /// <summary>
    /// Get Player Position 
    /// TO DO  : Addd More Data
    /// </summary>
    public class PlayerSharedData : MonoBehaviour
    {
        public static PlayerSharedData instance { get; private set; }

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(this);
            }
        }
    }
}

