﻿using System.Collections;
using UnityEngine;

namespace CarVsCops
{
    /// <summary>
    ///Playe rHeath
    /// Note : This script was inspired from the internet
    /// </summary>
    public class PlayerHealth : Health
    {
        private PlayerMovement playerMovement;
        private PlayerCarManager playerCarManager;
        //private CopsNearPlayer copsNearPlayer;
        private Rigidbody carRigidBody;

        public override void Start()
        {
            if (playerMovement == null && this.gameObject.GetComponent<PlayerMovement>() != null)
                playerMovement = this.gameObject.GetComponent<PlayerMovement>();

            if (playerCarManager == null && this.gameObject.GetComponent<PlayerCarManager>() != null)
                playerCarManager = this.gameObject.GetComponent<PlayerCarManager>();

            /*if (copsNearPlayer == null && this.gameObject.GetComponent<CopsNearPlayer>() != null)
                copsNearPlayer = this.gameObject.GetComponent<CopsNearPlayer>();*/

            if (carRigidBody == null && this.gameObject.GetComponent<Rigidbody>() != null)
                carRigidBody = this.gameObject.GetComponent<Rigidbody>();

            base.Start();
        }


        /// <summary>
        /// When Car Destroyed
        /// </summary>
        public override void DestroyTheCar()
        {
            if (playerCarManager != null)
                Destroy(playerCarManager.currentCar);

            if (playerMovement != null)
            {
                playerMovement.isCarCanMove = false;
            }

            if (carBoxCollider != null)
                carBoxCollider.isTrigger = false;


            if (carRigidBody != null)
            {
                carRigidBody.isKinematic = true;
                carRigidBody.velocity = Vector3.zero;
                carRigidBody.angularVelocity = Vector3.zero;
                carRigidBody.useGravity = false;
            }

            /*if (copsNearPlayer != null)
            {
                if (copsNearPlayer.playerCarCanvasManager != null)
                {
                    copsNearPlayer.playerCarCanvasManager.ActiveCanvasElements(PlayerCarCanvasManager.CanvasElements.HideAll);
                }
            }*/

            if (CopsCarSpawningManager.instance)
                CopsCarSpawningManager.instance.isPlayerDead = true;

            GameManager.instance.GameEnd();
        }

        /// <summary>
        /// Add Life
        /// </summary>
        public void AddLife()
        {
            if (currentHitsTaken >= maxHitToTake)
                return;

            currentHitsTaken++;
            if (ScoreManager.instance)
                ScoreManager.instance.SetLifes(currentHitsTaken);
        }

        /// <summary>
        /// Lose Life When Dead
        /// </summary>
        public override void LoseLife()
        {
            if (currentHitsTaken <= 0)
                return;
            if (ScoreManager.instance && !isdead)
                ScoreManager.instance.SetLifes(currentHitsTaken);
        }

        public override void SetIsInvulnerable()
        {
            isInvulnerable = true;
            base.SetIsInvulnerable();
        }

    }
}