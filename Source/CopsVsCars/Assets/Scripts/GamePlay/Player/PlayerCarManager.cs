﻿namespace CarVsCops
{
    /// <summary>
    /// Player Car Manager
    /// Note : This script was inspired from the internet
    /// </summary>
    public class PlayerCarManager : CarManager
    {
        private PlayerMovement playerCarManager;
        public override void Start()
        {
            if (this.gameObject.GetComponent<PlayerMovement>())
                playerCarManager = this.gameObject.GetComponent<PlayerMovement>();

            CarSettingsReader(currentCarID);
            CarMovementSettings(currentCarID);
        }

        /// <summary>
        /// Set Car Settings
        /// </summary>
        /// <param name="carID"></param>
        private void CarMovementSettings(int carID)
        {
            if (carID > carSettings.Capacity)
                return;

            if (playerCarManager != null)
            {
                playerCarManager.carSpeed = carSettings[carID].carSpeed;
                playerCarManager.carSteeringRoationSpeed = carSettings[carID].carSteeringRoationSpeed;
            }
        }
    }
}