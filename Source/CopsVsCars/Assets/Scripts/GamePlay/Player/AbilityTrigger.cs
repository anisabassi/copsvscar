﻿using UnityEngine;

namespace CarVsCops
{
    /// <summary>
    /// Acitve the combo UI when can near player
    /// </summary>
    public class AbilityTrigger : MonoBehaviour
    {
        [Header("Ability Trigger Configuration")]
        public SpecialAbility.Ability CurrentAbility;
        public float abilityDuration;
        public int coinsToAdd;
        public float boostSpeedToAdd;
        public float distanceMax = 100f;
        private float distance;
        /// <summary>
        /// Active Ability when collide with player
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.GetComponent<SpecialAbility>())
            {

                // Spawn New Ability Arround Player
                CollectibleSpawningManager.instance.SpawnNewCollectible(CurrentAbility);
                Destroy(this.gameObject);
            }
        }

        private void Update()
        {
            if (PlayerSharedData.instance == null)
                return;

            distance = Vector3.Distance(this.transform.position, PlayerSharedData.instance.transform.position);
            if (distance >= distanceMax)
            {
                CollectibleSpawningManager.instance.SpawnNewCollectible(CurrentAbility);
                Destroy(this.gameObject);
            }
        }
    }
}


