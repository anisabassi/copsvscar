﻿using System.Collections.Generic;
using UnityEngine;

namespace CarVsCops
{
    /// <summary>
    /// Parent class for the Car Manager behavior
    /// Note : This script was inspired from the internet
    /// </summary>
    public class CarManager : MonoBehaviour
    {
        [Header("Car Settings")]
        // List of car that can be used
        public List<CarDataSettings> carSettings = new List<CarDataSettings>();
        // Current Used Car
        public int currentCarID;
        // Current Car
        public GameObject currentCar;
        protected Health health;
        protected DamageOnTouch damageOnTouch;
        protected ColorBlinker colorBlinker;
        protected BoxCollider boxCollider;

        // Start is called before the first frame update
        public virtual void Start()
        {
            CarSettingsReader(currentCarID);
        }

        // Add new Car
        public virtual void CarSettingsReader(int carID)
        {
            if (carID > carSettings.Capacity)
                return;

            SpawnCar(carID);
        }

        /// <summary>
        /// Spawn The Car
        /// </summary>
        /// <param name="carID">Car Id To Spawn</param>
        public virtual void SpawnCar(int carID)
        {
            currentCar = Instantiate(carSettings[carID].carPrefab, transform.position, transform.rotation) as GameObject;

            currentCar.transform.parent = transform;

            currentCar.transform.localEulerAngles = carSettings[carID].rotationOnSpawn;

            currentCar.transform.localPosition = Vector3.zero;

            transform.localPosition = new Vector3(transform.position.x, transform.position.y + carSettings[carID].carPrefabHeightFromTheFloor, transform.position.z);


            SetCarData(carID);
        }

        /// <summary>
        /// Set Car Configuration from it"s data settings
        /// </summary>
        /// <param name="carID">Car Id To Spawn</param>
        public virtual void SetCarData(int carID)
        {
            if (this.gameObject.GetComponent<Health>())
            {
                health = this.gameObject.GetComponent<Health>();
                health.currentHitsTaken = carSettings[carID].health;
                health.maxHitToTake = carSettings[carID].health;
            }

            if (this.gameObject.GetComponent<DamageOnTouch>())

            {
                damageOnTouch = this.gameObject.GetComponent<DamageOnTouch>();

                damageOnTouch.damageHitGiven = carSettings[carID].damageGiven;
            }

            if (this.gameObject.GetComponent<ColorBlinker>())

            {
                colorBlinker = this.gameObject.GetComponent<ColorBlinker>();

                colorBlinker.SetupColors(currentCar.GetComponent<Renderer>().material);
            }

            if (this.gameObject.GetComponent<BoxCollider>())
            {
                boxCollider = this.gameObject.GetComponent<BoxCollider>();

                boxCollider.center = carSettings[carID].collisionCenter;
                boxCollider.size = carSettings[carID].colissionSize;
            }
        }
    }
}