﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarVsCops
{
    /// <summary>
    /// Scanne Cars in field of view
    /// Damge Cars inside field of view
    /// Note : This script was inspired from the internet
    /// </summary>
    public class FieldOfView : MonoBehaviour
    {
        [Header("View Configuration")]
        public float viewRadius;

        [Range(0, 360)]
        public float viewAngle;

        public LayerMask targetMask;
        public LayerMask obstacleMask;

        [Header("DAmage Configuration")]
        public bool isDamageVisibleTargets;

        public int damageAmount;

        [HideInInspector]
        public List<Transform> visibleTargets = new List<Transform>();

        /// <summary>
        /// Start Scanning for cars
        /// </summary>
        private void OnEnable()
        {
            StopAllCoroutines();
            StartCoroutine("FindTargetsWithDelay", .2f);
        }

        /// <summary>
        /// Scanne with dealy
        /// </summary>
        /// <param name="delay">Delay before scanning</param>
        /// <returns></returns>
        private IEnumerator FindTargetsWithDelay(float delay)
        {
            while (true)
            {
                yield return new WaitForSecondsRealtime(delay);
                FindVisibleTargets();
            }
        }

        /// <summary>
        /// Start Searching for visible target inside field of view
        /// </summary>
        private void FindVisibleTargets()
        {
            visibleTargets.Clear();

            // Get object with colliders inside sphere
            Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, targetMask);

            for (int i = 0; i < targetsInViewRadius.Length; i++)
            {
                // Check the detected object is inside the view angle
                Transform target = targetsInViewRadius[i].transform;
                Vector3 dirToTarget = (target.position - transform.position).normalized;
                if (Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2)
                {
                    // Check target in certain distance
                    float dstToTarget = Vector3.Distance(transform.position, target.position);

                    if (!Physics.Raycast(transform.position, dirToTarget, dstToTarget, obstacleMask))
                    {
                        // Add visble target to the list
                        visibleTargets.Add(target);
                    }
                }
            }

            // Damage Visible Targets
            if (isDamageVisibleTargets)
            {
                DamageVisibleTargets();
            }
        }

        /// <summary>
        /// Get Angle Direction this used in inspector GUI drow
        /// </summary>
        /// <param name="angleInDegrees">Angle</param>
        /// <param name="angleIsGlobal">is World Angle</param>
        /// <returns></returns>
        public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
        {
            if (!angleIsGlobal)
            {
                angleInDegrees += transform.eulerAngles.y;
            }
            return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
        }

        /// <summary>
        /// Damage All Visible Targets
        /// </summary>
        private void DamageVisibleTargets()
        {
            if (visibleTargets == null)
                return;

            isDamageVisibleTargets = false;
            // Sent Damage To all detected cops car that have a health
            foreach (Transform visibleTarget in visibleTargets)
            {
                if (visibleTarget.gameObject.GetComponent<CopsHeath>())
                {
                    visibleTarget.gameObject.GetComponent<CopsHeath>().TakeDamge(damageAmount);
                }
            }

            this.gameObject.SetActive(false);
        }

        /// <summary>
        /// Kill All Visible Targets
        /// </summary>
        public void KillAll()
        {
            isDamageVisibleTargets = true;
        }
    }
}