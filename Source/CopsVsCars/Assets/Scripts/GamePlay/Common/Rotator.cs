﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CarVsCops
{
    /// <summary>
    /// Rotate object continuously
    /// </summary>
    public class Rotator : MonoBehaviour
    {
        [Header("Rotation Configuration")]
        public GameObject objectToRotate;
        public float rotationSpeed;
        public Vector3 accesToRotate;
        // Update is called once per frame
        private void FixedUpdate()
        {
            if (objectToRotate != null)
                objectToRotate.transform.Rotate(new Vector3(accesToRotate.x * rotationSpeed * Time.unscaledDeltaTime,
                                                            accesToRotate.y * rotationSpeed * Time.unscaledDeltaTime,
                                                            accesToRotate.z * rotationSpeed * Time.unscaledDeltaTime));
        }
    }
}
