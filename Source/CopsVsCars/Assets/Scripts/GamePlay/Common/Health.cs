﻿using System.Collections;
using UnityEngine;

namespace CarVsCops
{
    /// <summary>
    /// Parent class for the health behavior
    /// Note : This script was inspired from the internet
    /// </summary>
    public class Health : MonoBehaviour
    {
        [Header("Health Configuration")]
        public bool isInvulnerable = false;

        public bool isdead;

        public int currentHitsTaken;
        [Header("Get Hit Configuration")]
        public bool isColliderActiveWhenGetHit;

        protected ColorBlinker colorBlinker;
        protected BoxCollider carBoxCollider;
        public float takingAHitDuration = 1f;
        public GameObject deathPartcile;
        public int maxHitToTake;
        // Start is called before the first frame update
        public virtual void Start()
        {

            if (colorBlinker == null && this.gameObject.GetComponent<ColorBlinker>() != null)
                colorBlinker = this.gameObject.GetComponent<ColorBlinker>();

            if (carBoxCollider == null && this.gameObject.GetComponent<BoxCollider>() != null)
                carBoxCollider = this.gameObject.GetComponent<BoxCollider>();
        }

        /// <summary>
        /// Sent damage to be taken
        /// </summary>
        /// <param name="damgeValue">damage value</param>
        public virtual void TakeDamge(int damgeValue)
        {
            if (isInvulnerable && isdead)
                return;

            currentHitsTaken -= damgeValue;

            // If taking hit then blink the color
            if (colorBlinker != null)
            {
                colorBlinker.StartBlinking(false);
                StopCoroutine("TakeAHit");
                StartCoroutine("TakeAHit");
            }

            // If Can't take any more hit then destroy and spawn death particle
            if (currentHitsTaken <= 0)
            {
                if (deathPartcile)
                {
                    GameObject tempParticle = Instantiate(deathPartcile, transform.position, transform.rotation) as GameObject;
                }
                isdead = true;
                DestroyTheCar();
            }

            // Sent
            LoseLife();
        }

        /// <summary>
        /// Take A Hit Behavior
        /// </summary>
        /// <returns></returns>
        private protected IEnumerator TakeAHit()
        {
            
            SetIsInvulnerable();

            // Disable Collider
            if (carBoxCollider != null && !isColliderActiveWhenGetHit)
                carBoxCollider.enabled = false;

            // Start Color Blinking
            colorBlinker.StartBlinking(true);
            yield return new WaitForSeconds(takingAHitDuration);
            colorBlinker.StartBlinking(false);
            if (carBoxCollider != null && !isColliderActiveWhenGetHit)
                carBoxCollider.enabled = true;
        }

        /// <summary>
        /// Destroy the car
        /// </summary>
        public virtual void DestroyTheCar()
        {
        }


        public virtual void LoseLife()
        {
        }

        /// <summary>
        /// Set Is Invulnerable
        /// </summary>
        public virtual void SetIsInvulnerable()
        {
        }
    }
}