﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarVsCops
{
    /// <summary>
    /// Destroy Current Object with dealy
    /// </summary>
    public class DestroyWithDelay : MonoBehaviour
    {

        public float durationBeforeDestruction;
        // Start is called before the first frame update
        void Start()
        {
            StartCoroutine("Destruction");
        }

        /// <summary>
        /// Destroy
        /// </summary>
        /// <returns></returns>
        IEnumerator Destruction()
        {
            yield return new WaitForSeconds(durationBeforeDestruction);
            Destroy(this.gameObject);
        }
    }
}
