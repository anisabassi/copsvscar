﻿using UnityEngine;

namespace CarVsCops
{
    /// <summary>
    /// Parent class for the Car Movement behavior
    /// Note : This script was inspired from the internet
    /// </summary>
    public class CarMovement : MonoBehaviour
    {
        [Header("Car CarMovement Configuration")]
        public float carSpeed;

        public float carSteeringRoationSpeed;

        public enum Direction { Front, Left, Right, Back };

        public Direction carDirectionChosen;

        public Vector3 carDirection;
        protected Rigidbody carRigidbody;
        public bool isCarCanMove;

        public virtual void Start()
        {
            if (carRigidbody = this.GetComponent<Rigidbody>())
                carRigidbody = this.GetComponent<Rigidbody>();

            SetDirection();
        }

        // Update is called once per frame
        public virtual void FixedUpdate()
        {
            if (!isCarCanMove)
                return;

            MoveCar();
            RotateCar();
        }

        /// <summary>
        /// Move Car Forward
        /// </summary>
        public virtual void MoveCar()
        {
            carRigidbody.velocity = transform.forward * carSpeed;
        }

        /// <summary>
        /// Rotate Car
        /// </summary>
        public virtual void RotateCar()
        {
        }

        /*/// <summary>
        /// Ca
        /// </summary>
        public virtual void CalculateAngle()
        {
        }*/

        /// <summary>
        /// Set Car Direction
        /// </summary>
        public virtual void SetDirection()
        {
            switch (carDirectionChosen)
            {
                case Direction.Back:
                    carDirection = Vector3.back;
                    break;

                case Direction.Front:
                    carDirection = Vector3.forward;
                    break;

                case Direction.Left:
                    carDirection = Vector3.left;
                    break;

                case Direction.Right:
                    carDirection = Vector3.right;
                    break;

                default:
                    carDirection = Vector3.forward;
                    break;
            }
        }
    }
}