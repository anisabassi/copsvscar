﻿using UnityEngine;

namespace CarVsCops
{
    /// <summary>
    /// Cars Settings
    /// </summary>
    [CreateAssetMenu(menuName = "Create Car")]
    public class CarDataSettings : ScriptableObject
    {
        public GameObject carPrefab;

        [Header("Car Setting Configuration")]
        public float carSpeed;
        public float carSteeringRoationSpeed;
        public int health;
        public int damageGiven;

        [Header("Car Transform Configuration")]
        public float carPrefabHeightFromTheFloor;
        public Vector3 collisionCenter;
        public Vector3 colissionSize;
        public Vector3 rotationOnSpawn;
    }
}