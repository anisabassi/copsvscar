﻿using System.Collections;
using UnityEngine;

namespace CarVsCops
{
    /// <summary>
    /// Sent Damage When toutching other object with health
    /// </summary>
    public class DamageOnTouch : MonoBehaviour
    {
        [Header("Damage Configuration")]
        // Damage value to be giving
        public int damageHitGiven;
        // Rate of giving damage when keep colliding with other objects
        public float damageGivingRateWhenKeepToutching = 0.5f;
        // The layers that will be damaged by this object
        public LayerMask targetLayerMask;
        private Health targetHealth;
        private bool isKeepDamaging = true;

        /// <summary>
        /// If Collide with anothe object that have heath and is the target layers list the send damge
        /// </summary>
        /// <param name="collision"></param>
        private void OnCollisionStay(Collision collision)
        {
            if (((1 << collision.gameObject.layer) & targetLayerMask) != 0)
            {
                if (collision.gameObject.GetComponent<Health>() != null && isKeepDamaging)
                {
                    collision.gameObject.GetComponent<Health>().TakeDamge(damageHitGiven);
                    isKeepDamaging = false;
                    StartCoroutine("GiveDamage");
                }
            }
        }

        /// <summary>
        /// Keep Giving Damage With dealy rate
        /// </summary>
        /// <returns></returns>
        private IEnumerator GiveDamage()
        {

            yield return new WaitForSecondsRealtime(damageGivingRateWhenKeepToutching);
            isKeepDamaging = true;

        }
    }
}