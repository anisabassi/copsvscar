﻿using UnityEngine;

namespace CarVsCops
{
    /// <summary>
    /// Blink Material Color
    /// Note : This script was inspired from the internet
    /// </summary>
    public class ColorBlinker : MonoBehaviour
    {
        [Header("Blink Configuration")]
        public float fadeDuration = 1f;

        public Color color1 = Color.red;
        public Color color2 = Color.white;

        private Color startColor;
        private Color endColor;
        private float lastColorChangeTime;

        public int materialID;
        public Material material;
        private Color originalColor;
        public bool isStartBlinking = false;
        private Color tempColor = Color.white;
        private float colorRatio;

        /// <summary>
        /// Start blinking
        /// </summary>
        private void Update()
        {
            if (isStartBlinking && material != null)
            {
                // Fade current color the the next color
                colorRatio = (Time.time - lastColorChangeTime) / fadeDuration;
                colorRatio = Mathf.Clamp01(colorRatio);
                material.color = Color.Lerp(startColor, endColor, colorRatio);

                // If reach the final color
                if (colorRatio == 1f)
                {
                    // Reset chnage timer
                    lastColorChangeTime = Time.time;

                    // Switch colors
                    tempColor = startColor;
                    startColor = endColor;
                    endColor = tempColor;
                }
            }
        }

        /// <summary>
        /// Start Blinking
        /// </summary>
        /// <param name="start"></param>
        public void StartBlinking(bool start)
        {
            //SetupColors();
            isStartBlinking = start;
            if (!start)
                material.color = originalColor;
        }

        /// <summary>
        /// SetUp Color to blink between
        /// </summary>
        /// <param name="sentMaterial"></param>
        public void SetupColors(Material sentMaterial)
        {
            material = sentMaterial;
            if (material != null)
            {
                startColor = color1;
                endColor = color2;
                originalColor = material.color;
            }
        }
    }
}