﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarVsCops
{
    /// <summary>
    /// Cops Car Manager
    /// </summary>
    public class CopsHeath : Health
    {

        /// <summary>
        /// Destory the Object 
        /// </summary>
        public override void DestroyTheCar()
        {
            Destroy(this.gameObject);
            base.DestroyTheCar();
        }

        /// <summary>
        /// When Object is destroyed Send Crashing score and spawn New car
        /// </summary>
        private void OnDestroy()
        {
            if (CopsCarSpawningManager.instance != null)
            {
                ScoreManager.instance.AddCarCrashingScore();
                CopsCarSpawningManager.instance.spawnCount--;
            }

        }
    }
}
