﻿using UnityEngine;

namespace CarVsCops
{
    public class AICopsMovement : CarMovement
    {
        // Update is called once per frame
        public override void FixedUpdate()
        {
            if (!isCarCanMove && PlayerSharedData.instance != null)
                return;

            base.FixedUpdate();
        }

        public override void RotateCar()
        {
            Vector3 pointTarget = transform.position - PlayerSharedData.instance.transform.position;

            pointTarget.Normalize();

            float value = Vector3.Cross(pointTarget, transform.forward).y;

            carRigidbody.angularVelocity = carSteeringRoationSpeed * value * new Vector3(0, 1, 0);
        }
    }
}