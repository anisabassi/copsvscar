﻿namespace CarVsCops
{
    /// <summary>
    /// Cops Car Manager
    /// </summary>
    public class CopsCarManager : CarManager
    {
        private AICopsMovement aiCopsMovement;

        // Start is called before the first frame update
        public override void Start()
        {
            if (this.gameObject.GetComponent<AICopsMovement>())
                aiCopsMovement = this.gameObject.GetComponent<AICopsMovement>();

            CarMovementSettings(currentCarID);

            base.Start();
        }

        /// <summary>
        /// Set Car Settings
        /// </summary>
        /// <param name="carID"></param>
        private void CarMovementSettings(int carID)
        {
            if (carID > carSettings.Capacity)
                return;

            if (aiCopsMovement != null)
            {
                aiCopsMovement.carSpeed = carSettings[carID].carSpeed;
                aiCopsMovement.carSteeringRoationSpeed = carSettings[carID].carSteeringRoationSpeed;
            }
        }
    }
}